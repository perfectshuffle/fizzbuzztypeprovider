﻿namespace TestTP

open Microsoft.FSharp.Core.CompilerServices
open Samples.FSharp.ProvidedTypes

[<AutoOpen>]
module FizzBuzz =
  let fizzbuzz n =
    match n with
    | x when x % 15 = 0 -> "Fizz Buzz"
    | x when x % 5 = 0 -> "Buzz"
    | x when x % 3 = 0 -> "Fizz"
    | _ -> string n

[<TypeProvider()>]
type FizzBuzzProvider() as this =
  inherit TypeProviderForNamespaces()

  // Get the assembly and namespace used to house the provided types
  let asm = System.Reflection.Assembly.GetExecutingAssembly()
  let ns = "Troll.Your.Interviewer.With"

  let rec getType n =
    let typeName = fizzbuzz n
    
    let t = ProvidedTypeDefinition(typeName, Some(typeof<obj>))
    t.AddMemberDelayed (fun() -> getType (n+1))
    t.HideObjectMethods <- true
    
    let ctor = ProvidedConstructor([])
    ctor.InvokeCode <- fun args ->
      <@@
        for i = 1 to n do
          printfn "%s" (fizzbuzz i)
        typeName
      @@>
    t.AddMember ctor

    t

  let rootType = ProvidedTypeDefinition(asm, ns, "FizzBuzz", Some (typeof<obj>), HideObjectMethods = true)

  do rootType.AddMember (getType 1)
  do this.AddNamespace(ns, [rootType])
  
[<assembly:TypeProviderAssembly>] 
do()
